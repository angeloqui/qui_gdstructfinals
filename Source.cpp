#include <iostream>
#include <string>
#include <time.h>

#include "Stack.h"
#include "Queue.h"

using namespace std;

int getArraySize() {
	
	int size;
	
	cout << "Enter size for element sets: ";
	cin >> size;
	
	return size;
}

void pushElements(Stack<int>& stack, Queue<int>& queue) {
	
	int n;
	cout << "\n\nEnter number: ";
	cin >> n;
	
	stack.push(n);
	queue.push(n);

	cout << "\n\nTop element of sets: ";
	cout << "\nQueue: " << queue.top();
	cout << "\nStack: " << stack.top();
}

void popElements(Stack<int>& stack, Queue<int>& queue) {
	
	cout << "\n\nYou have popped the front elements.";

	stack.pop();
	queue.pop();
}

void displayArray(Stack<int>& stack, Queue<int>& queue) {
	
	cout << "Queue elements:" << endl;
	for (int i = 0; i < queue.getSize(); i++)
		cout << queue[i] << endl;
	
	cout << "Stack elements:" << endl;
	for (int i = 0; i < stack.getSize(); i++)
		cout << stack[i] << endl;
	
	stack.empty();
	queue.empty();
}

int main() {

	int size = getArraySize();
	Stack<int> stack(size);
	Queue<int> queue(size);

	int option;
	cout << string(2, '\n');

	while (true) {
		
		cout << "What do you want to do?";
		cout << "\n1 - Push elements";
		cout << "\n2 - Pop elements";
		cout << "\n3 - Print everything then empty set" << endl;

		cin >> option;

		if (option == 1) {
			pushElements(stack, queue);
		} else if (option == 2) {
			popElements(stack, queue);
		} else if (option == 3) {
			displayArray(stack, queue);
		}
		
		cout << endl;
		system("pause");
		system("cls");
	}
}
